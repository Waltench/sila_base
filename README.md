**Important**: This code, in its current form, is mainly to give active SiLA 2 WG Members and other interested parties a reference point. It might not comply with the latest version of the Standard, and much of its content might change in the future.

The first release candidate of SiLA 2 is scheduled for mid 2018.

# SiLA Standard
SiLA’s mission is to establish international standards which create open connectivity in lab automation. SiLA’s vision is to create interoperability, flexibility and resource optimization for laboratory instruments integration and software services based on standardized communication protocols and content specifications. SiLA promotes open standards to allow integration and exchange of intelligent systems in a cost effective way.

The SiLA 2 specification is a multi part specification and the work-in-progress
documents can be accessed on google drive:

* [Part (A) - Overview, Concepts and Core Specification](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM/edit)
* [Part (B) - Mapping Specification](https://docs.google.com/document/d/1-shgqdYW4sgYIb5vWZ8xTwCUO_bqE13oBEX8rYY_SJA/edit)
* [Part (C) - Features Index](https://docs.google.com/document/d/1J9gypD6HofLQZ8cPgLWljRuO0V8l5dS22TWQxFy4bhY/edit)

For more information, visit our [website](http://sila-standard.com/).

In case of general questions, contact either Max ([max@unitelabs.ch](mailto:max@unitelabs.ch)) or Daniel ([daniel.juchli@sila-standard.org](mailto:daniel.juchli@sila-standard.org)).

# SiLA Base
This repository contains base definitions of the SiLA standard, such as the feature schemas and framework 
protos. Additionally it contains the SiLA Features of the standardization group.

## Style Guidelines
The tabbing in the xml documents is done with 2 spaces.

# License
This code is licensed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License)